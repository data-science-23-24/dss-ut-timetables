import  pandas  as  pd

def read_csv(
    file_path : str,
    separator : str =',',
    header : str ='infer',
    index_col=None,
    encoding : str ='utf-8'
    ) -> pd.DataFrame:
    try:
        df = pd.read_csv(file_path, sep=separator, header=header, index_col=index_col, encoding=encoding)
        return df
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
        return None
    except Exception as e:
        print(f"Error: An error occurred while parsing the CSV file: {e}")
        return None

def read_xlsx(
    file_path: str
    ) -> pd.DataFrame:
    try:
        df = pd.read_excel(file_path)
        return df
    except FileNotFoundError:
        print(f"Error: File '{file_path}' not found.")
        return None
    except Exception as e:
        print(f"Error: An error occurred while parsing the CSV file: {e}")
        return None

def rename_dataframe_columns(
    dataframe: pd.DataFrame,
    new_column_names : list
    ) -> pd.DataFrame:
    try:
        if len(dataframe.columns) != len(new_column_names):
            raise ValueError("Number of new column names should match the number of columns in the DataFrame.")
        renamed_dataframe = dataframe.copy()
        renamed_dataframe.columns = new_column_names
        return renamed_dataframe
    except Exception as e:
        print(f"Error: Columns could not be updated: {e}")
        return None


def shard_dataframe(
    df : pd.DataFrame,
    columns_to_remove : list[str]
    ) -> tuple[pd.DataFrame]:
    try:
        removed_df = df[columns_to_remove]
        remaining_df = df.drop(columns=columns_to_remove)
        return remaining_df, removed_df
    except ValueError:
        print(f"Error: arguments do not adhere to needed conditions")
        return None
    except Exception as e:
        print(f"Error: An error occurred while sharding {e}")
        return None